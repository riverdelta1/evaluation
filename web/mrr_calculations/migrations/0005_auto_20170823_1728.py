# -*- coding: utf-8 -*-
# Generated by Django 1.10.6 on 2017-08-23 17:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('mrr_calculations', '0004_auto_20170823_1556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='clientmrr',
            name='customer',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='customer_mrr', to='mrr_calculations.Customer'),
        ),
    ]
