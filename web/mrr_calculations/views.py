import datetime
import json

from dateutil import relativedelta

from django.views.generic import TemplateView
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.core import serializers
from django.core.serializers.json import DjangoJSONEncoder

from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator

from mrr_calculations.forms import CsvUploadForm
from mrr_calculations.mrr_processing import process_csv, query_mrr, query_bookings, process_new_mrr
from mrr_calculations.models import CsvUpload, InvoiceItem, ClientMrr, Customer

@method_decorator(login_required, name='dispatch')
class UploadCsvView(TemplateView):
    template_name = "mrr_calculations/upload_csv.html"

    def get(self, request):
        # IF THERE IS UNCLASSIFIED MRR
        # DISPLAY THAT INSTEAD OF THE UPLOAD FORM
        form = CsvUploadForm()
        invoice_items = InvoiceItem.objects.filter(is_processed=False)
        return render(request, self.template_name, {'form':form, 'invoice_items':invoice_items})

    def post(self, request):
        form = CsvUploadForm(request.POST, request.FILES)
        if form.is_valid():
            try:
                invoice_rows = process_csv(request.FILES['file'], request.FILES['file'].name)
                return render(request, self.template_name, {'form':CsvUploadForm(), 'invoice_items':invoice_rows})
            except ValueError as e:
                form.add_error(None, repr(e))


        return render(request, self.template_name, {'form':form})


@method_decorator(login_required, name='dispatch')
class UpdateCsvView(TemplateView):

    def post(self, request, tr_id):
        invoice_items = InvoiceItem.objects.filter(id=int(tr_id))
        updated_invoice_item = invoice_items[0]
        updated_invoice_item.start_date = request.POST.get("start_date")
        updated_invoice_item.end_date = request.POST.get("end_date")
        updated_invoice_item.product_class = request.POST.get("product_class")
        updated_invoice_item.revenue_type = request.POST.get("revenue_type")
        updated_invoice_item.save()

        data = serializers.serialize('json', invoice_items)
        return HttpResponse(data, content_type='application/json')

@method_decorator(login_required, name='dispatch')
class ProcessView(TemplateView):

    def get(self, request):
        process_new_mrr()
        return HttpResponseRedirect('/upload_csv/')

from datetime import datetime

@method_decorator(login_required, name='dispatch')
class AnalyticsView(TemplateView):
    template_name = "mrr_calculations/analytics.html"

    def get(self, request):
        kwargs = {}
        start_date, end_date = None, None

        if 'start_date' in request.GET:
            start_date = datetime.strptime(request.GET['start_date'], '%m/%d/%Y').date()

        if 'end_date' in request.GET:
            end_date = request.GET['end_date']

        # Query for the bookings data and stick it in our list
        mrr_list = query_bookings(start_date,end_date,None, **kwargs)

        if 'format' in request.GET and request.GET['format'] == 'json':
            return HttpResponse(json.dumps(mrr_list, cls=DjangoJSONEncoder))
        else:
            return render(request, self.template_name, {'mrr_types':mrr_list})


@method_decorator(login_required, name='dispatch')
class ScatterView(TemplateView):
    template_name = "mrr_calculations/scatter.html"

    def get(self, request):
        kwargs = {}
        start_date, end_date = None, None

        if 'client' in request.GET:
            kwargs['customer_id'] = int(request.GET['client'])
        if 'start_date' in request.GET:
            start_date = request.GET['start_date']
        if 'end_date' in request.GET:
            end_date = request.GET['end_date']

        # Query for the bookings data and stick it in our list
        mrr_list = query_bookings(start_date,end_date,None, **kwargs)

        if 'format' in request.GET and request.GET['format'] == 'json':
            return HttpResponse(json.dumps(mrr_list, cls=DjangoJSONEncoder))
        else:
            return render(request, self.template_name, {'mrr_types':mrr_list})


@method_decorator(login_required, name='dispatch')
class AccountsView(TemplateView):
    template_name = "mrr_calculations/accounts.html"

    def get(self, request):
        customers = Customer.objects.extra(select={'case_insensitive_title': 'lower(name)'}).order_by('case_insensitive_title')
        return render(request, self.template_name, {'customers':customers})


class SingleAccountMrrView(TemplateView):
    def get(self, request, account_id):
        return HttpResponse(json.dumps({'success':True}), content_type='application/json')
