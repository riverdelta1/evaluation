$(document).ready( function() {

	// Pulling data from html
	var x_data = [];
	var y_data_bookings = [];
	var y_data_new = [];
	var y_data_churn = [];
	var y_data_expansion = [];
	var y_data_net_new = [];
	var y_data_total = [];
	$("#mrr-breakdown p").each(function() {
		x_data.push($(this).attr("data-x-label"));
		y_data_bookings.push($(this).attr("data-bookings"));
		y_data_new.push($(this).attr("data-new"));
		y_data_churn.push($(this).attr("data-churn"));
		y_data_expansion.push(parseFloat($(this).attr("data-expansion")) + parseFloat($(this).attr("data-contraction")));
		y_data_net_new.push(parseFloat($(this).attr("data-expansion")) + parseFloat($(this).attr("data-contraction")) + parseFloat($(this).attr("data-new")) + parseFloat($(this).attr("data-churn")));
		y_data_total.push($(this).attr("data-total"));
	});

	y_data_bookings = roundArray(y_data_bookings);
    y_data_new = roundArray(y_data_new);
	y_data_churn = roundArray(y_data_churn);
    y_data_expansion = roundArray(y_data_expansion);
    y_data_net_new = roundArray(y_data_net_new);
    y_data_total = roundArray(y_data_total);

	var y_data_bookings_2 = y_data_bookings.slice();
	var y_data_new_2 = y_data_new.slice();
	var y_data_churn_2 = y_data_churn.slice();
	var y_data_expansion_2 = y_data_expansion.slice();
	var y_data_net_new_2 = y_data_net_new.slice();
	var y_data_total_2 = y_data_total.slice();

	y_data_bookings_2 = roundArray(y_data_bookings_2, "commas");
	y_data_new_2 = roundArray(y_data_new_2,"commas");
	y_data_churn_2 = roundArray(y_data_churn_2,"commas");
	y_data_expansion_2 = roundArray(y_data_expansion_2,"commas");
	y_data_net_new_2 = roundArray(y_data_net_new_2,"commas");
	y_data_total_2 = roundArray(y_data_total_2,"commas");

	// For Graph 1
	var chart_1 = $("#sample-chart-1");
	var grid_1 = $("#sample-grid-1");
	var title = "Bookings";
	// var x_data = ["Mar-16", "Apr-16", "May-16", "Jun-16", "Jul-16", "Aug-16", "Sep-16", "Oct-16", "Nov-16", "Dec-16", "Jan-16", "Feb-16"];  // X axis
	var y_data_1 = y_data_bookings; //[39668, 109485, 40250, 61685, 37437, 34413, 23262, 47645, 3001, 71023, 27062, 39499]; // Y axis
	var y_type_1 = 'bar';
	var y_title_1 = "Bookings";
	var y_color_1 = 'rgba(52, 152, 219, 1.0)'; // Blue
	var y_stack_1 = 1;

	var y_matrix = [{type: y_type_1, label: y_title_1, data: y_data_1, backgroundColor: y_color_1, stack: y_stack_1}];
	var y_matrix_grid = [{type: y_type_1, label: y_title_1, data: y_data_bookings_2, backgroundColor: y_color_1, stack: y_stack_1}];

	barGraph(chart_1, title, x_data, y_matrix);
	insertGrid(grid_1, x_data, y_matrix_grid);


	// For Graph 2
	var chart_2 = $("#sample-chart-2");
	var grid_2 = $("#sample-grid-2");
	var title = "Period MRR (New, Churn Upgrade, Downgrade)";
	// var x_data = ["Mar-16", "Apr-16", "May-16", "Jun-16", "Jul-16", "Aug-16", "Sep-16", "Oct-16", "Nov-16", "Dec-16", "Jan-16", "Feb-16"];  // X axis
	var y_data_1 = y_data_new; //[1689, 2874, 2798, 2441, 757, 627, 351, 0, 0, 3319, 187, 581]; // Y axis
	var y_type_1 = 'bar';
	var y_title_1 = "New";
	var y_color_1 = 'rgba(46, 204, 113, 1.0)'; // Green
	var y_stack_1 = 1;

	var y_data_2 = y_data_churn; //[0, -318, 0, -132, 0, -252, 0, 0, 0, -168, -148, -459]; // Y axis
	var y_type_2 = 'bar';
	var y_title_2 = "Churn";
	var y_color_2 = 'rgba(231, 76, 60, 1.0)'; // Red
	var y_stack_2 = 2;

	var y_data_3 = y_data_expansion; //[1140, -280, -407, 565, 406, 377, -75, -65, 604, -317, 59, 1104]; // Y axis
	var y_type_3 = 'bar';
	var y_title_3 = "Expansion";
	var y_color_3 = 'rgba(52, 152, 219, 1.0)'; // Blue
	var y_stack_3 = 3;

	var y_data_4 = y_data_net_new; //[2829, 2276, 2391, 2874, 1163, 751, 277, -65, 604, 2834, 98, 1226]; // Y axis
	var y_type_4 = 'line';
	var y_title_4 = "Net New";
	var y_color_4 = 'rgba(52, 73, 94, 1.0)'; // Black
	var y_stack_4 = 4;

	var y_matrix = [{type: y_type_1, label: y_title_1, data: y_data_1, backgroundColor: y_color_1, stack: y_stack_1},
					{type: y_type_2, label: y_title_2, data: y_data_2, backgroundColor: y_color_2, stack: y_stack_2},
					{type: y_type_3, label: y_title_3, data: y_data_3, backgroundColor: y_color_3, stack: y_stack_3},
					{type: y_type_4, label: y_title_4, data: y_data_4, backgroundColor: 'rgba(0,0,0,0)', pointBackgroundColor: "rgba(52, 73, 94, 1.0)", borderColor: y_color_4, stack: y_stack_4}];
	var y_matrix_grid = [{type: y_type_1, label: y_title_1, data: y_data_new_2, backgroundColor: y_color_1, stack: y_stack_1},
						 {type: y_type_2, label: y_title_2, data: y_data_churn_2, backgroundColor: y_color_2, stack: y_stack_2},
						 {type: y_type_3, label: y_title_3, data: y_data_expansion_2, backgroundColor: y_color_3, stack: y_stack_3},
						 {type: y_type_4, label: y_title_4, data: y_data_net_new_2, backgroundColor: 'rgba(0,0,0,0)', pointBackgroundColor: "rgba(52, 73, 94, 1.0)", borderColor: y_color_4, stack: y_stack_4}];


	barGraph(chart_2, title, x_data, y_matrix);
	insertGrid(grid_2, x_data, y_matrix_grid);


	// For Graph 3
	var chart_3 = $("#sample-chart-3");
	var grid_3 = $("#sample-grid-3");
	var title = "Total MRR";
	// var x_data = ["Mar-16", "Apr-16", "May-16", "Jun-16", "Jul-16", "Aug-16", "Sep-16", "Oct-16", "Nov-16", "Dec-16", "Jan-16", "Feb-16"];  // X axis
	var y_data_1 = y_data_total; // [23275, 25551, 27942, 30816, 31979, 32731, 33007, 32942, 33546, 36380, 36479, 37705]; // Y axis
	var y_type_1 = 'bar';
	var y_title_1 = "Total MRR";
	var y_color_1 = 'rgba(46, 204, 113, 1.0)'; // Green
	var y_stack_1 = 1;

	var y_matrix = [{type: y_type_1, label: y_title_1, data: y_data_1, backgroundColor: y_color_1, stack: y_stack_1}];
	var y_matrix_grid = [{type: y_type_1, label: y_title_1, data: y_data_total_2, backgroundColor: y_color_1, stack: y_stack_1}];

	barGraph(chart_3, title, x_data, y_matrix);
	insertGrid(grid_3, x_data, y_matrix_grid);
});



/* Function: Create a graph out of the variables coming in */
/* Can handle mixed graphs.
   Parameters: chart_canvas: ID of where graph should be placed
   	           title: not used right now,
			   x_data: x-axis labeling
			   y_matrix: contains data, color, look, of the y-axis */
function barGraph(chart_canvas, title, x_data, y_matrix) {
	var myChart = new Chart(chart_canvas, {
		type: 'bar',
		data: {
	    	labels: x_data,
	        datasets: y_matrix
	    },
	    options: {
	        scales: {
	            yAxes: [{
	                stacked: true
	            }]
	        }
	    }
	});
}
